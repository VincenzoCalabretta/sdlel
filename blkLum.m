%funzione per la costruzione di uno spettro di luminosita' per un corpo
%nero
res=1000;
freqRange=linspace(0,2500,res).*10^-9;
T=6050;


%General spectrum
Llambda=zeros(1,res);
for i=1:res
    Llambda(i)=blkBdyLum(freqRange(i),T);
end
plot(freqRange,Llambda);hold on;
%Particolar calc.
Lout=blkBdyLum(905e-9,T);
plot(905*10^-9,Lout,'sr');
Lout=blkBdyLum(405e-9,T) 
plot(405*10^-9,Lout,'sg');

function [Llambda]=blkBdyLum(lambda,T)
    h=6.62607004e-34; %costante di Plank
    k=1.3806488e-23;%costante di Boltzmann
    c=3*10^8;
    Llambda-=2*h*c^2/(lambda^5)/(exp(h*c/lambda/k/T)-1);
end