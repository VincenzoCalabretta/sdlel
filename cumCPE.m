%Funzione per il calcolo della funzione cumulativa della velocita'o del
%diametro dei detriti
%Vincenzo Calabretta 2021-03-06

%La funzione cumulativa invoca la funzione di integrazione tante volte quante 
%sia la risoluzione e calcola l integrale fino a tale punto

%Si potrebbe considerare di far caricare in memoria la lettura del file per evitare di far rileggere
%al parser il file di testo molte volte di fila.

%Nota: creare un file generico di configurazione per tutte le funzioni CPE

function outCum=cumCPE(AMcode,pick)
    [~,binSupEdg,~]=fdpCPE(AMcode,pick);
    res=size(binSupEdg,2);
    for iter=1:res
        outCum(iter)=intFdpCPE(AMcode,pick,0,binSupEdg(iter));
    
    end
end