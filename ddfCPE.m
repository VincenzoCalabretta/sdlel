%Funzione per il calcolo della funzione di probabilita' della velocita'
%o del diametro dei detriti in funzione di un determinante
%Vincenzo Calabretta 2021-03-09

function [fdpOut,binSupEdg,binInfEdg]=ddfCPE(AMcode,pick)
    %ottenimento delle strutture dati {Diam,Vel}
    hIn=histCPE(AMcode);
    totInt=sum(hIn{pick}.Values(:));
    fdpOut=hIn{pick}.Values(:)'./totInt;
    binSupEdg=hIn{pick}.BinEdges(2:size(hIn{pick}.BinEdges,2));
    binInfEdg=hIn{pick}.BinEdges(1:size(hIn{pick}.BinEdges,2)-1);
end