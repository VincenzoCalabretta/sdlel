%Function to evaluate EtaB for a given mirror horizontal frequency
%vertical dispersion angle and maximum detection range

%input di funzione freq, thV e MDR
function etaB=evalEtaB(freq,thV,MDR)
    % configuration parameters
    AMcode=31;
    rangeRes=20;
    
    rangeDom=linspace(0,MDR,rangeRes);
    etaBr=zeros(1,rangeRes-1);
    etaB=0;
    
    % main
    for i=1:rangeRes-1
        etaBr(i)=rendFreqCPE(AMcode,freq,thV,rangeDom(i));
        etaB=etaB+etaBr(i)*(rangeDom(i+1)^2-rangeDom(i)^2)/(rangeDom(rangeRes)^2);
    end
    

end