%Costruttore di funizone di distribuzione di probabilita' 
%a partire da output CPE analisi master mediante fitting della distr.
%Vincenzo Calabretta 2021-03-06

function hOut=histCPE(AMcode)
	%definizione preliminare di valori ausiliari
    nIntervDiam=10; %numero intervalli desiderati distr diametro
    nIntervVel=20;  %numero intervalli desiderati distr velocita'
    limInfDiam=0.001;%limite inferiore distr. diametri [m]
    limSupDiam=0.01; %limite superiore distr. diametri [m]
    limInfVel=0;    %limite inferiore distr. velocita' [Km/s]
    limSupVel=20;   %limite superiore distr. velocita' [Km/s]
    
    binSDiam=linspace(limInfDiam,limSupDiam,nIntervDiam+1);
    binSVel=linspace(limInfVel,limSupVel,nIntervVel+1);
    
    %ottenimento struttura dati da file cpe
    CPE=parserCPE(AMcode); 
    valDiam=CPE.diam;   %Valori diametro cpe
    valVel=CPE.vel;     %Valorcli velocita' cpe
    molt=CPE.flxval;    %Moltiplicatori per il contributo al flusso

    figure(1);
    hDiam = neoHist(valDiam,molt,nIntervDiam,binSDiam);
    title('Diameter DDF');
    xlabel('Debris diameter [m]');ylabel('2D flux distribution [1/m^2/yr]');
    
    figure(2);
    hVel = neoHist(valVel,molt,nIntervVel,binSVel);
    title('Velocity DDF');
    xlabel('Debris velocity [Km/s]');ylabel('2D flux distribution [1/m^2/yr]');
    hOut={hDiam,hVel};


end










