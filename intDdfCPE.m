%Funzione per il calcolo dell'integrale della fdp velocita' o del
%diametro dei detriti in funzione di un intervallo
%Vincenzo Calabretta 2021-03-06
function totProb=intDdfCPE(AMcode,pick,intLow,intUpp)
    toll=[0.001,0.2];
    %ottenimento delle strutture dati {Diam,Vel}
    hIn=histCPE(AMcode);
    %trovare indici estremi di integrazione
    ddLow = find(intLow-hIn{pick}.BinEdges<toll(pick),1); %trova il piu vicino
    ddUpp = find(intUpp-hIn{pick}.BinEdges<toll(pick),1)-1;
    
    
    outInt=sum(hIn{pick}.Values(ddLow:ddUpp));
    totInt=sum(hIn{pick}.Values(:));
    totProb=outInt/totInt;
    
    figure(3);
    plot(hIn{pick}.Values(:)','r');hold on;
    plot(ddLow:ddUpp,hIn{pick}.Values(ddLow:ddUpp),'-g');
end
