close all;clear;
soglia=zeros(1,1000)+32;
[~,numPhotA]=lpb(0.01);
[~,numPhotB]=lpb(0.008);
[~,numPhotC]=lpb(0.006);
[~,numPhotD]=lpb(0.004);
[~,numPhotE]=lpb(0.002);
[range,numPhotF]=lpb(0.001);
figure(1)
semilogy(range,numPhotA,'linewidth',1);
hold on;grid on;
semilogy(range,numPhotB,'linewidth',1);
semilogy(range,numPhotC,'linewidth',1);
semilogy(range,numPhotD,'linewidth',1);
semilogy(range,numPhotE,'linewidth',1);
semilogy(range,numPhotF,'linewidth',1);
semilogy(range,soglia,'linewidth',2);
legend('0.01 m','0.008 m','0.006 m','0.004 m','0.002 m','0.001 m','threshold')
xlabel('range [m]');ylabel('N photons received');
title('link budget equation for N')
function [range,numPhot]=lpb(diam)
sensorNum=1;
fillFactor=0.78;
Arec=0.045^2*pi/4/sensorNum*fillFactor;
etaSysRicev=0.90;
lambda=905e-9;
rhoDebris=0.3;
rhoMems=0.90;

h=6.63e-34;
c=3e8;

pulseTime=2e-9; 
pulseFreq=3e5; 
lsUptime=pulseFreq*pulseTime;
lsTotalPower=1.6;
phi=737.23;
pulsePower=lsTotalPower/lsUptime;
lasDiv=7e-3; %0.4deg


Et=pulseTime*pulsePower;

range=linspace(1,150,1000);

sigma=diam^2*pi/4;
Aill=(lasDiv*range).^2*pi/4;

receivPower=zeros(1,1000);
numPhot=zeros(1,1000);
for i=1:size(range,2)
    receivPower(i)=pulsePower*sigma*Arec*etaSysRicev/(Aill(i)*pi*range(i)^2)*rhoDebris*rhoMems;
end
for i=1:size(range,2)
    %numPhot(i)=lambda*sigma*Arec*etaSysRicev*Et/(h*c*Aill(i)*pi*range(i)^2)*rhoDebris*rhoMems;
    numPhot(i)=phi*pulseTime*lambda*sigma*Arec*etaSysRicev*rhoDebris*rhoMems/(h*c*pi*range(i)^2);
end
end



