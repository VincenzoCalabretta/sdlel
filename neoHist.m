%Funzione di costruzione istogramma

% CPE=parserCPE(22);
% val=CPE.vel;
% molt=CPE.flxval;


function hOut=neoHist(val,molt,n,binS)
%     binMin=min(val); 
%     binMax=max(val);
%     binS=linspace(binMin,binMax,n+1);
%NOTA 2021-03-21 E' stato necessario definire bins manuali per i diametri

    binTop=binS(2:n+1);%definition of bins characteristics
    binBot=binS(1:n);


   
    binVal=zeros(1,n);
    for i=1:n
       ind1=find( val >= binBot(i) & val < binTop(i));
       binVal(i)=sum(molt(ind1));
    end
    bar(binVal);
    xticks(1:n)
    for i=1:n
        binLabels{i}=strcat(num2str(binBot(i)),' - ',num2str(binTop(i)));
    end
    xticklabels(binLabels)
    
    hOut.BinEdges=binS;
    hOut.Values=binVal;
end