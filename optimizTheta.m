%Optimization script to determine optimal thetaV,thetaH

clear;clc;
%AM histogram database preload
AMcode=31;
hIn=histCPE(AMcode);
save('AM_HistDB');

%General parameters
thH_resolution=10;
thH_upper=deg2rad(10);
thH_domInterv=[9E-6 thH_upper]; %Interval to evaluate, between diffr limited and 10 deg
thV_resolution=12;
thV_upper=deg2rad(60);
thV_domInterv=[9E-6 thV_upper]; %Interval to evaluate, between diffr limited and 10 deg
alpha_subV=deg2rad(60);

%Pulse frequency calculation parameters
alpha_subH=deg2rad(60);
k=20;
%Irradiance calculation parameters
eta_source=0.0372;
eta_opt=0.9;
P_o=88;
P_abs=1.6;
deltaT_pulse=2e-9;

%Frame refresh rate
freqF_resolution=100;
freqF_domInterv=[100,1000];


etaB_target=0.9;		%minimum EtaB specification
MDR=100;			%Maximum Detection Range

thH=linspace(thH_domInterv(1),thH_domInterv(2),thH_resolution);
thV=linspace(thV_domInterv(1),thV_domInterv(2),thV_resolution);
freq=linspace(freqF_domInterv(1),freqF_domInterv(2),freqF_resolution);

% variables allocation
etaB=zeros(thH_resolution,thV_resolution);
freqF_result=zeros(thH_resolution,thV_resolution);
freqP_result=zeros(thH_resolution,thV_resolution);
phi_result=zeros(thH_resolution,thV_resolution);
freqMH_result=zeros(thH_resolution,thV_resolution);
freqMV_result=zeros(thH_resolution,thV_resolution);
for thH_iter = 1:thH_resolution
    for thV_iter = 1:thV_resolution
        for freqF_iter=1:freqF_resolution
            %Required framerate calculation
            etaB(thH_iter,thV_iter)=evalEtaB(freq(freqF_iter),alpha_subV,MDR);
            freqF_result(thH_iter,thV_iter) = freq(freqF_iter);
            %Mirror horizontal and vertical frequency calculation
            freqMV_result(thH_iter,thV_iter)=freqF_result(thH_iter,thV_iter);
            freqMH_result(thH_iter,thV_iter)=alpha_subV/thV(thV_iter)*freqMV_result(thH_iter,thV_iter);
            %Required pulse frequency calculation
            freqP_result(thH_iter,thV_iter) = alpha_subH*k*2*pi*...
                freqMH_result(thH_iter,thV_iter)/thH(thH_iter);
            %Target specific calculation
%            phi_result(thH_iter,thV_iter)=eta_source*eta_opt*P_abs/deltaT_pulse/...
%               (thH(thH_iter)*thV(thV_iter)*freqP_result(thH_iter,thV_iter));
             phi_result(thH_iter,thV_iter)=P_o/...
                 (thH(thH_iter)*thV(thV_iter));
            
            if etaB(thH_iter,thV_iter) >= etaB_target
                break;
            end
        end
    end
end
save('optThetaOUT')









