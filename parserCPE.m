%Parser per la analisi di file di output CPE ottenuti
%da master
%Vincenzo Calabretta - 2020-03-06
function CPE=parserCPE(AMcode)
    AMstr = strcat('AM',num2str(AMcode));
    %path = strcat(pwd,'\projectsAM\',AMstr,'\output','\master_cond.cpe');
    path = strcat(pwd,'/projectsAM/',AMstr,'/output','/master_cond.cpe');
    delimiterIn = ' ';
    headerlinesIn = 36;
    raw = importdata(path,delimiterIn,headerlinesIn);
    CPE.diam = raw.data(:,6);
    CPE.vel = raw.data(:,8);
    CPE.flxval=raw.data(:,2);
end