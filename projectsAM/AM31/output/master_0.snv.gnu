###########################
#                         #
# Use right mouse key for #
#     STORE and REPLOT    #
#                         #
###########################
set encoding default
set nogrid
set key below Left reverse
set autoscale x
set autoscale y
set nologscale x
set nologscale y
set format x "%g"
set format y "%g"
set format z "%g"
set format cb "%7.1e"
set xtics
set ytics
set ztics
set xyplane 0.05
set xlabel "Surface Normal Velocity [km/s]"
set ylabel "Object Flux [1/m^2/yr]"
unset log cb
unset pm3d
unset hidden3d
set view  60, 30, 1, 1
set title "ESA-MASTER Model v8.0.2 \n2D flux distribution vs. Surface Normal Velocity"
plot \
   "master_0.snv" using 1:($16) title "" with histeps linetype 1, \
   "master_0.snv" using 1:($17) title "" with histeps linetype 7, \
   "master_0.snv" using 1:($16)+($17)+($18) title "" with histeps linetype 12
