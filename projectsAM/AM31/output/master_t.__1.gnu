###########################
#                         #
# Use right mouse key for #
#     STORE and REPLOT    #
#                         #
###########################
set encoding default
set nogrid
set key below Left reverse
set size ratio 0.7
set nokey
set autoscale x
set autoscale y
set autoscale z
set nologscale x
set nologscale y
set nologscale z
set format x "%g"
set format y "%g"
set format z "%g"
set format cb "%7.1e"
set xtics
set ytics
set ztics
set xyplane 0.05
set xlabel "Impact Elevation [deg]" offset 0,-1
set ylabel "Impact Azimuth [deg]" offset 1,-1
set label "Object Flux [1/m^2/yr]" at graph 0, graph 0, graph 1.1
unset log cb
unset pm3d
set view 60,30,1,1
set hidden3d
set cblabel "Object Flux [1/m^2/yr]"
set title "ESA-MASTER Model v8.0.2 \n3D flux distribution vs. Impact Elevation and Impact Azimuth"
splot 'master_t.__1' using 1:2:20 with lines linetype 1 notitle
