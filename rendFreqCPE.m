%Funzione per il calcolo della efficenza di detezione relativa alla freq
%di aggiornamento

function rendFreq=rendFreqCPE(AMcode,freq,thetaV,range)
    %   range=100; %Default test values
    %   thetaV=deg2rad(2);
    lungDetez=range*thetaV;
    t=1/freq;
    %ottenimento delle strutture dati {Diam,Vel}
    %hIn=histCPE(AMcode); vecchio, ora si utilizza il preload
    load('AM_HistDB');
    totInt=sum(hIn{2}.Values(:));
    fdpPrelim=hIn{2}.Values(:)'./totInt;
    
    binSupEdg=hIn{2}.BinEdges(2:size(hIn{2}.BinEdges,2));
    binInfEdg=hIn{2}.BinEdges(1:size(hIn{2}.BinEdges,2)-1);
    allBin(:,1)=binSupEdg;
    allBin(:,2)=binInfEdg;
    speed=mean(allBin',1)*1000; %#ok<UDIM>
    
    
    fdpOut=fdpPrelim;
    for i=1:size(fdpPrelim,2)
        if(speed(i)*t>lungDetez)
            fdpOut(i)=fdpPrelim(i)*lungDetez/(t*speed(i));
        end
    end
    rendFreq=sum(fdpOut);
    
    %graficatura
%     figure();
%     plot(fdpOut,'-s'); hold on;
%     plot(fdpPrelim,'-*');
%     legend('VelDDF*p(x)','VelDDF')
%     xlabel('km/s')
    
    

end